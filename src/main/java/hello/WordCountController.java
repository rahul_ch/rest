package hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path="/wordcount")
//Question 2
public class WordCountController {
    /*
     curl -X POST http://localhost:8080/wordcount -d '{"text":"a hello world world w"}' -H "Content-Type: application/json"
    */
    @PostMapping(path = "")
    public @ResponseBody List<WordCount> wordcount(@RequestBody WordCountRequest request) {
        Map<String, Long> counts = new HashMap<>();
        for (String word : request.getText().split("\\s+")) {
            Long n = counts.get(word);
            n = (n == null) ? 1 : ++n;
            counts.put(word, n);
        }
        return counts
            .entrySet()
            .stream()
            .map(e -> new WordCount(e.getKey(), e.getValue()))
            .sorted((e1, e2) -> -1*e2.getWord().compareTo(e1.getWord()))
            .collect(Collectors.toList());
    }
}