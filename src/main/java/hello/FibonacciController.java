package hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Controller
@RequestMapping(path="/fibonacci")
//Question 3
public class FibonacciController {
    /*
     curl http://localhost:8080/fibonacci?n=10
    */

    public Long fibonacciNumber(Long n) {
        if(n <= 1)
            return 0l;
        if(n == 2)
            return 1l;
        return fibonacciNumber(n-1) + fibonacciNumber(n-2);
    }

    @GetMapping(path = "")
    public @ResponseBody List<Long> fibonacciNumbers(@RequestParam Long n) {
        return LongStream.range(1, n).map(i -> fibonacciNumber(i)).boxed().collect(Collectors.toList());
    }
}