package hello;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(path="/posts")
//Question 6
public class RestClientController {
    /*
     curl http://localhost:8080/posts
    */
    @GetMapping(path = "")
    public @ResponseBody List<Post> posts() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Post>> rateResponse =
            restTemplate.exchange("https://jsonplaceholder.typicode.com/posts",
            HttpMethod.GET, null, new ParameterizedTypeReference<List<Post>>() {
        });
        return rateResponse.getBody();
    }
}